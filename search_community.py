import api

def find_all_entities_in_community(service_type,
                                   community_city,
                                   community_state):
    all_entities = api.Service.get_all(service_type)
    all_entities = all_entities["results"]["facilityList"]

    all_entities = [_ for _ in all_entities if _["activeInd"] == "Y"]

    entities = []

    i = 0

    print(f"{len(all_entities)} to check")
    for entity in all_entities:
        if i % 25 == 0:
            print(f"{i} / len(all_entities)")
        i += 1

        entity_details = api.Service.entity_details(service_type,
                                                    entity['id'])
        entity_details = entity_details["results"]["facility"]

        if (entity_details["communityCity"] == community_city and
                entity_details["communityState"] == community_state):
            entities.append(entity_details)

    return entities
