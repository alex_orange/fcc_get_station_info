import requests
import json

api_base_uri = "https://publicfiles.fcc.gov/api/"

class Service:
    sub_uri = "service/"
    service_type_values = ["tv", "fm", "am", "cable", "sdars", "dbs"]
    formats = ["json", "xml"]

    @classmethod
    def get_all(cls, service_type, format_="json"):
        assert service_type in cls.service_type_values
        assert format_ in cls.formats
        endpoint = f"{service_type}/facility/getall.{format_}"

        result = requests.get(api_base_uri + cls.sub_uri + endpoint)

        assert result.status_code == 200

        return json.loads(result.text) if format_ == "json" else result.text

    @classmethod
    def entity_details(cls, service_type, entity_id, format_="json"):
        assert service_type in cls.service_type_values
        assert format_ in cls.formats
        endpoint = f"{service_type}/facility/id/{entity_id}.{format_}"

        result = requests.get(api_base_uri + cls.sub_uri + endpoint)

        assert result.status_code == 200

        return json.loads(result.text) if format_ == "json" else result.text
