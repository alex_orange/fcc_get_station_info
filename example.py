#import search_community

#salt_lake_city_results = search_community.find_all_entities_in_community(
#    "tv", "SALT LAKE CITY", "UT")

# Run get_all_details.py first to generate json file.
import json

with open("tv_all_entity_details.json", "r") as in_file:
    data = json.load(in_file)

queries = [
    ("SALT LAKE CITY", "UT"),
    ("RALEIGH", "NC"),
    ("DES MOINES", "IA"),
]

for city, state in queries:
    print(city, state)
    results = []

    for entity in data:
        if (entity["communityCity"] == city and
                entity["communityState"] == state):
            results.append((entity['frequency'], entity["rfChannel"],
                            entity['callSign']))

    results.sort()

    for _ in results:
        print(_)
