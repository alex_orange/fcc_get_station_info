import json

import api


service_type = "tv"


all_entities = api.Service.get_all(service_type)
all_entities = all_entities["results"]["facilityList"]

all_entities = [_ for _ in all_entities if _["activeInd"] == "Y"]

entities = []

i = 0

print(f"{len(all_entities)} to retrieve")
for entity in all_entities:
    if i % 25 == 0:
        print(f"{i} / len(all_entities)")
    i += 1

    entity_details = api.Service.entity_details(service_type,
                                                entity['id'])
    entity_details = entity_details["results"]["facility"]

    entities.append(entity_details)

with open(f"{service_type}_all_entity_details.json", "w") as out_file:
    json.dump(entities, out_file)
